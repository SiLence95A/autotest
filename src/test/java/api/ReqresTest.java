package api;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;

public class ReqresTest {
    private final static String URL = "https://reqres.in/";

    @Test
    public void chekAvatarAndTest (){

        Specification.installSpecification(Specification.requestSpec(URL), Specification.responseSpec200());
        List<UserData> users = given()
                .when()
                .get("api/users?page=2")
                .then().log().all()
                .extract().body().jsonPath().getList("data",UserData.class);

        users.forEach(x->Assertions.assertTrue(x.getAvatar().contains(x.getId().toString())));

       Assertions.assertTrue(users.stream().allMatch(x->x.getEmail().endsWith("@reqres.in")));

        List<String> avatars = users.stream().map(UserData::getAvatar).collect(Collectors.toList());
        List<String> ids = users.stream().map(x->x.getId().toString()).collect(Collectors.toList());

        for (int i=0; i<avatars.size(); i++){
           Assertions.assertTrue(avatars.get(i).contains(ids.get(i)));
        }
    }

    @Test
    public void succesRegTest(){
        Specification.installSpecification(Specification.requestSpec(URL), Specification.responseSpec200());
        Integer id =4;
        String token = "QpwL5tke4Pnpja7X4";
        Register user = new Register("eve.holt@reqres.in","pistol");
        SuccessReg successReg = given()
                .body(user)
                .when()
                .post("api/register")
                .then().log().all()
                .extract().as(SuccessReg.class);
        Assertions.assertEquals(id,successReg.getId());
        Assertions.assertEquals(token,successReg.getToken());

    }

    @Test
    public void unSuccessRegTest () {
        Specification.installSpecification(Specification.requestSpec(URL), Specification.responseSpec400());
        Register user = new Register("sydney@fife","");
        UnSuccesReg unSuccesReg = given()
                .body(user)
                .post("api/register")
                .then()
                .extract().as(UnSuccesReg.class);
        Assertions.assertEquals("Missing password", unSuccesReg.getError());
    }

    @Test
    public void sortYearsTests (){
        Specification.installSpecification(Specification.requestSpec(URL), Specification.responseSpec200());
        List<ColorsdData> colors = given()
                .when()
                .get("api/unkown")
                .then().log().all()
                .extract().body().jsonPath().getList("data",ColorsdData.class);
        List<Integer> years = colors.stream().map(ColorsdData::getYear).collect(Collectors.toList());
        List<Integer> sortYears = years.stream().sorted().collect(Collectors.toList());
        Assertions.assertEquals(sortYears,years);
        System.out.println(years);
        System.out.println(sortYears);
    }

    @Test
    public void deleteUserTests (){
        Specification.installSpecification(Specification.requestSpec(URL), Specification.responseSpecUnique(204));
        given()
                .when()
                .delete("api/usrers/2")
                .then().log().all();
    }
}
