package hh;

import core.TestBase;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class HhTest extends TestBase {

    private final static  String URL = "https://hh.ru/applicant/resumes/view?resume=1edf0c93ff095811d20039ed1f6a3638497073";

    @Test
    public void chekAttributesHashMap (){
        HhResumePage hhResumePage = new HhResumePage(URL);
        Map<String, Object> expentedAttributes = new HashMap<>();
        expentedAttributes.put(HhResumePage.GENDER, "М");
        expentedAttributes.put(HhResumePage.AGE, 26);
        expentedAttributes.put(HhResumePage.CITY, "Санкт-Петербург");
        expentedAttributes.put(HhResumePage.CONFIRMED_PHONE, true);
        expentedAttributes.put(HhResumePage.READY_TO_RELOCATE, false);
        Map<String, Object> actualAttributes = hhResumePage.getAttributes();
        Assertions.assertEquals(expentedAttributes,actualAttributes);
    }

    @Test
    public void chekAttributesClass(){
        HhResumePage hhResumePage = new HhResumePage(URL);
        Resume expetedAttributes = new Resume("М",26,"Санкт-Петербург",
                true,false);
        Resume actualAttributes = new Resume(hhResumePage.getGennderYard(),hhResumePage.getAge(),
                hhResumePage.getCity(),hhResumePage.isPhoneConfirmed(),hhResumePage.isreadyToRelocate());
        Assertions.assertTrue(EqualsBuilder.reflectionEquals(expetedAttributes,actualAttributes));
    }


}
