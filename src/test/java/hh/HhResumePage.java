package hh;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Selenide.$x;

public class HhResumePage {
    private final SelenideElement gender = $x("//span[@data-qa='resume-personal-gender']");
    private final SelenideElement age = $x("//span[@data-qa='resume-personal-age']");
    private final SelenideElement city = $x("//span[@data-qa='resume-personal-address']");
    private final SelenideElement liveData = $x("//span[@data-qa='resume-personal-address']/ancestor::p");
    private final SelenideElement tick = $x("//div[@data-qa='resume-contacts-phone']/span[1]");

    public static String GENDER = "Пол";
    public static String AGE = "Возраст";
    public static String CITY = "Город";
    public static String CONFIRMED_PHONE = "Подтвержденный номер";
    public static String READY_TO_RELOCATE = "Готовность к переезду";


    public HhResumePage(String url) {
        Selenide.open(url);
    }

    public Map<String,Object> getAttributes (){
       return new HashMap<>() {
           {
               put(GENDER, getGennderYard());
               put(AGE, getAge());
               put(CITY, getCity());
               put(CONFIRMED_PHONE, isPhoneConfirmed());
               put(READY_TO_RELOCATE, isreadyToRelocate());
           }
       };

    }

    public boolean isreadyToRelocate (){
        return !liveData.getText().split(", ")[1].equals("не готов к переезду");
    }

    public boolean isPhoneConfirmed ()
    {
        return tick.isDisplayed();
    }
   public String getCity (){
        return city.getText();
   }

    public int getAge() {
        return Integer.parseInt(age.getText().replaceAll("\\D+", ""));
    }

    public String getGennderYard() {
        return gender.getText().equals("Мужчина") ? "М" : "Ж";
    }
}
